# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

places = Place.create([{ name: 'Blue Ousters'}, { name: 'Bad Coyote' }])
places.each do |r|
  (1..5).each{ r.tables << Table.create }
end
