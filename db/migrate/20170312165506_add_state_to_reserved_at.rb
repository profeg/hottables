class AddStateToReservedAt < ActiveRecord::Migration
  def change
    add_column :reserved_ats, :state, :string
  end
end
