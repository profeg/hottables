class CreateReservedAts < ActiveRecord::Migration
  def change
    create_table :reserved_ats do |t|
      t.belongs_to :table, index: true
      t.datetime :begin_time
      t.timestamps null: false
    end
  end
end
