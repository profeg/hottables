class CreateTables < ActiveRecord::Migration
  def change
    create_table :tables do |t|
      t.belongs_to :place, index: true
      t.timestamps null: false
    end
  end
end
