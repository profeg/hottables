class ReservedAtsController < ApplicationController
  def create
    begin_time = Time.parse(params[:begin_time])
    table_id = params[:place_id]
    reserved_at = ReservedAt.new(table_id: table_id, begin_time: begin_time, state: 'reserved')
    if reserved_at.save
      render json: { result: :ok }
    else
      render json: reserved_at.errors
    end
  end
end
