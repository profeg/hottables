class TablesController < ApplicationController
  def show
    render json: list_reserve_times(params[:id])
  end

  private
    def list_reserve_times(table_id, date = Time.now)
      table = Table.includes(:reserved_ats).find(table_id)
      start_hour = date.min > 30 ? date.hour + 1 : date.hour
      curr_time = Time.new(date.year, date.month, date.day, start_hour)
      list = []
      loop do
        reserved_at = table.reserved_ats.find_by(begin_time: curr_time)
        reserved_at ||= ReservedAt.new(begin_time: curr_time, table_id: table.id, state: 'free')
        list << reserved_at
        break if curr_time.hour == 23 && curr_time.min == 30
        curr_time = curr_time + 30.minutes
      end
      list
    end
end
