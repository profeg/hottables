class PlacesController < ApplicationController
  def index
    @places = Place.all
    render json: @places
  end

  def show
    @place_tables = Place.includes(:tables).find(params[:id]).tables
    render json: @place_tables
  end
end
