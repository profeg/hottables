class ReservedAt < ActiveRecord::Base
  belongs_to :table

  validate :reserved_time
  validate :clashing_reserved_time

  def reserved_time
    return true if begin_time > Time.current
    errors.add(:base, "can't be in past time")
  end

  def clashing_reserved_time
    return true if table.reserved_ats.where(begin_time: begin_time).blank?
    errors.add(:base, "this time already reserved")
  end

end
